import scipy as sp
from scipy import sparse
import matplotlib.pyplot as plt


def simulate_Q(Q, s_0=0, time_end=10, n_traj=1):
    trajs = []
    Q = sp.array(Q)
    sp.fill_diagonal(Q, 0)
    N = len(Q)

    # if (not n_traj == 1):
    if (not hasattr(s_0, '__iter__')):
        s_0 = sp.array([s_0] * n_traj)
    if (not hasattr(time_end, '__iter__')):
        time_end = sp.array([time_end] * n_traj)
    if (len(s_0) != n_traj) or (len(time_end) != n_traj):
        print("Lengths of s_0 and time_end do not match n_traj")
        return

    for i in range(n_traj):
        s = s_0[i]
        t = 0
        traj = []

        while t < time_end[i]:
            traj += [(t, s)]

            rate = sp.sum(Q[s])
            if rate == 0:
                t_sojourn = time_end[i] - t
            else:
                t_sojourn = sp.random.exponential(scale=1 / rate)
                p_vec = Q[s] / rate
                s = sp.random.choice(sp.arange(N), p=p_vec)

            t += t_sojourn

        traj += [(time_end[i], traj[-1][-1])]
        trajs += [sp.array(traj)]
    return trajs


def probability_ss(trajs, N, t_lin):
    # PROBABILITY DISTRIBUTION OVER STATES IN TIME
    n_traj = len(trajs)
    ticks = len(t_lin)

    p_ss = sp.zeros((ticks, N))
    trajs_res = sp.zeros((n_traj, ticks))

    for j, tr in enumerate(trajs):
        len_tr = len(tr)
        t_last = 0
        for i in range(ticks):
            for t in range(t_last, len_tr - 1):
                if tr[t, 0] <= t_lin[i] < tr[t + 1, 0]:
                    p_ss[i, int(tr[t, -1])] += 1
                    trajs_res[j][i] = int(tr[t, -1])
                    t_last = t
                    break
            else:
                p_ss[i, int(tr[-1, -1])] += 1
                trajs_res[j][i] = int(tr[-1, -1])
                t_last = len_tr
    p_ss /= n_traj

    return p_ss, trajs_res


def probability_ss_subset(trajs, N, t_lin, subset):
    # Only a subset of the state space is kept distinct.
    # While a trajectory is outside this subset,
    # it is no longer considered as part of the sum.

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME
    n_traj = len(trajs)
    ticks = len(t_lin)

    p_ss = sp.zeros((ticks, N))
    trajs_res = sp.zeros((n_traj, ticks))

    for j, tr in enumerate(trajs):
        len_tr = len(tr)
        t_last = 0
        for i in range(ticks):
            for t in range(t_last, len_tr - 1):
                if tr[t, 0] <= t_lin[i] < tr[t + 1, 0]:
                    s_ind = int(tr[t, -1])
                    if s_ind in subset:
                        p_ss[i, subset.index(s_ind)] += 1
                    trajs_res[j][i] = s_ind
                    t_last = t
                    break
            else:
                s_ind = int(tr[-1, -1])
                if s_ind in subset:
                    p_ss[i, subset.index(s_ind)] += 1
                trajs_res[j][i] = s_ind
                t_last = len_tr
    p_ss = (p_ss.T / p_ss.sum(axis=1)).T

    return p_ss, trajs_res


if __name__ == '__main__':
    # pepa_fname = 'PEPA_test.generator'
    # pepa_fname = 'PEPA_TOMP_4-4_less.generator'
    # pepa_fname = 'PEPA_TOMP_4-4_2560.generator'
    pepa_fname = 'PEPA_TOMP_2-2-1_416.generator'
    # pepa_fname = 'PEPA_WS_3-3-2-2_1376.generator'
    # pepa_fname = 'PEPA_TOMP_3-3-3_10752.generator'
    # pepa_fname = 'PEPA_TOMP_4-3-3_23552.generator'

    # LOADING Q MATRIX
    gen = sp.loadtxt(pepa_fname, delimiter=',')

    row = (gen[:, 0] - 1).astype(int)
    col = (gen[:, 1] - 1).astype(int)
    data = gen[:, -1]
    N = max(sp.hstack((row, col))) + 1
    print(N)
    Q = sparse.csr_matrix((data, (row, col)),
                          shape=(N, N)).toarray()

    # DRAWING MULTIPLE TRAJECTORIES
    n_traj = 10 ** 5
    t_end = 200

    trajs = simulate_Q(Q, time_end=t_end, n_traj=n_traj)

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME
    ticks = 100
    t_lin = sp.linspace(0, t_end, ticks)
    p_ss = probability_ss(trajs, N=N, t_lin=t_lin)

    # PLOTTING HEATMAP
    plt.figure()
    plt.imshow(p_ss.T,
               cmap='hot', interpolation='none', vmin=0, vmax=0.5,
               extent=[0, t_end, N, 0], aspect='auto')
    plt.colorbar()
    plt.ylabel("State index")
    plt.xlabel("Time (s)")
    plt.title(str("Probability heatmap over time for " + pepa_fname +
                  ".\n n_trajectories = " + str(n_traj)))
    plt.savefig(str(pepa_fname + '_hist.png'))
    plt.show()
