import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt

"""
================
GRID AUTO-GRAPH
================
population = [N, N]
G = nx.grid_graph(dim=population)
G = nx.DiGraph(G)

print(nx.adjacency_matrix(G).getnnz())
print(nx.adjacency_matrix(G))

graph_pos = nx.spectral_layout(G)
edge_labels = {(k1, k2): v for k1, k2, v in G.edges(data='weight')}
nx.draw_networkx(G, pos=graph_pos, edge_labels=edge_labels)

"""

# ============================
# MANUALLY CONSTRUCTING GRAPH
# ============================


def sp_SIR(N=15):
    system_label = 'SIRS'
    G = nx.DiGraph()

    """
    SIRS model
    infection:      S + I -> 2I, q_inf
    recovery:       I -> R,      q_rec
    susceptibility: R -> S,      q_sus

    System is closed (fixed population size).
    """

    # nodes
    nodes = [(i, j, k)
             for i in range(N + 1) for j in range(N + 1) for k in range(N + 1)
             if i + j + k == N]

    # edges
    # q_inf = 2
    # q_rec = 1
    def q_inf(i, j, k):
        return 0.1 * i * j / (i + j + k)

    def q_rec(i, j, k):
        return 0.05 * j  # ** 2

    def q_sus(i, j, k):
        return 0.01 * k

    e_inf = [((i, j, k), (i - 1, j + 1, k), {'weight': q_inf(i, j, k)})
             for i, j, k in nodes if (i - 1, j + 1, k) in nodes]

    e_rec = [((i, j, k), (i, j - 1, k + 1), {'weight': q_rec(i, j, k)})
             for i, j, k in nodes if (i, j - 1, k + 1) in nodes]

    e_sus = [((i, j, k), (i + 1, j, k - 1), {'weight': q_sus(i, j, k)})
             for i, j, k in nodes if (i + 1, j, k - 1) in nodes]

    G.add_edges_from(e_inf)
    G.add_edges_from(e_rec)
    G.add_edges_from(e_sus)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)

    def d_sus(x):
        return q_sus(*x) - q_inf(*x)

    def d_inf(x):
        return q_inf(*x) - q_rec(*x)

    def d_rec(x):
        return q_rec(*x) - q_sus(*x)

    rates = [d_sus, d_inf, d_rec]

    def ode_grad(x, t0):
        g = sp.array([r(x) for r in rates])
        return g

    return G, ode_grad, system_label


if __name__ == '__main__':
    G, ode_grad, system_label = sp_SIR(N=50)
    nodelist = list(G.nodes())
    Q = sp.asarray(nx.adjacency_matrix(
        G, weight='weight', nodelist=nodelist).todense())
    sp.set_printoptions(precision=3)
    # print(Q)

    import simulate_ss
    from scipy.integrate import odeint

    pop_total = sp.sum(nodelist[0])
    t_end = 100
    n_traj = 100

    S_frac = 8 * pop_total // 10
    I_frac = 2 * pop_total // 10
    R_frac = pop_total - (S_frac + I_frac)

    s_0 = nodelist.index((S_frac, I_frac, R_frac))

    trajs = simulate_ss.simulate_Q(Q, s_0=s_0, time_end=t_end, n_traj=n_traj)

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME (DISCRETE SS)
    ticks = 1000
    t_lin = sp.linspace(0, t_end, ticks)
    p_ss, trajs = simulate_ss.probability_ss(trajs,
                                             N=len(nodelist), t_lin=t_lin)
    print("Probability over SS sum, should be 1")
    print(sp.amin(sp.sum(p_ss, axis=1)), sp.amax(sp.sum(p_ss, axis=1)))

    # PROJECTING DISCRETE P_ss TO CONTINUOUS DOMAIN
    n_dim = len(nodelist[0])
    graph_pos = {node: sp.array(node) for node in nodelist}
    p_R_ss = sp.zeros((ticks, n_dim))
    p_R_ss = p_ss @ sp.array([graph_pos[i] for i in nodelist])

    # ODE solution
    s_pos = graph_pos[nodelist[s_0]]  # / pop_total
    traj_ode = odeint(ode_grad, s_pos, t_lin)
    # traj_ode *= pop_total

    # Plotting
    fig = plt.figure()
    axarr = fig.add_subplot(111)
    linstys = ['-', '--', ':']
    labels = ['S', 'I', 'R']
    for d in range(n_dim):
        axarr.plot(t_lin, p_R_ss[:, d],
                   color='blue', linestyle=linstys[d],
                   label=f'{labels[d]} SSA mean')
    for d in range(n_dim):
        axarr.plot(t_lin, traj_ode[:, d],
                   color='red', linestyle=linstys[d],
                   label=f'{labels[d]} ODE')
    axarr.set_xlabel('time (s)')
    axarr.set_ylabel('Species count')
    axarr.legend()
    axarr.set_title('Species evolution\n'
                    f'({system_label}, $s_0={nodelist[s_0]}$)')
    plt.show()
