import scipy as sp
from scipy import linalg as la
from scipy.integrate import odeint

import sdeint
from sdeint import itoint

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import networkx as nx

import embed_ss
import simulate_ss
import compare_ss


# Checking hitting time for CTMC trajectories
def fpt_ctmc(trajs, barrier):
    n_traj = len(trajs)
    times = sp.zeros(n_traj)
    for i, tr in enumerate(trajs):
        # if the trajectory doesn't reach within given time, mark with -1
        times[i] = -1
        for t, s in tr:
            if s in barrier:
                times[i] = t
                break
    return times


# Checking hitting time for fluid trajectory
def fpt_fluid(traj, barrier, eps=None, graph_pos=None):
    t_lin = traj[:, 0]
    traj_s = traj[:, 1:]
    for t, s in zip(t_lin, traj_s):
        if eps is not None:
            barrier_pos = sp.array([graph_pos[idx] for idx in barrier])
            diff = barrier_pos - s
            diff = la.norm(diff, axis=1)
            if sp.any(diff < eps):
                return t
        elif eps is None and graph_pos is not None:
            diff = graph_pos - s
            diff = la.norm(diff, axis=1)
            idx_min = sp.argmin(diff)
            if idx_min in barrier:
                return t
    else:
        return -1


def fpt_compare_pCTMC(pCTMC_sys, pop_total=50):
    # """
    # LOADING CTMC
    # G, Q, nodelist = compare_ss.load_PEPA()
    G, ode_grad, _ = pCTMC_sys(N=pop_total)
    nodelist = list(G.nodes())
    Q = sp.asarray(nx.adjacency_matrix(G, nodelist=nodelist).todense())
    sp.fill_diagonal(Q, -sp.sum(Q - sp.diag(sp.diag(Q)), axis=1))
    # Embedding in \mathbb{R}^{n_dim}

    n_dim = 2
    # """

    print()
    print("Embedding graph and GP for gradient field...")

    graph_pos, GPR, Poynting_vec_field, Diffusion_matrix_dict = embed_ss.embed_graph(
        G, proj_dim=n_dim, plot=False, plot_eigenvalues=False)

    # Classical fluid embedding (in species counts / concentration space)
    graph_pos_ode = {node: sp.array(node) for node in nodelist}

    # DRAWING MULTIPLE TRAJECTORIES (DISCRETE SS)
    n_traj = 1 * 10 ** 3
    t_end = 20

    # SIR --- initial state
    """
    pop_total = sp.sum(nodelist[0])
    S_frac = 8 * pop_total // 10
    I_frac = 2 * pop_total // 10
    R_frac = pop_total - (S_frac + I_frac)

    s_0 = nodelist.index((S_frac, I_frac, R_frac))
    # """

    # LV --- initial state
    # """
    prey_num = 3 * pop_total // 10
    pred_num = 7 * pop_total // 10
    s_0 = nodelist.index((prey_num, pred_num))
    # """

    print()
    print("Simulating from Q, n_traj =", n_traj)

    trajs = simulate_ss.simulate_Q(Q, s_0=s_0, time_end=t_end, n_traj=n_traj)

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME (DISCRETE SS)
    ticks = int(t_end / 0.01)
    t_lin = sp.linspace(0, t_end, ticks)

    # FLUID INTEGRATION PATHS (ours and standard ODE)
    traj_fluid = sp.zeros((ticks, n_dim))
    traj_ode = sp.zeros((ticks, len(nodelist[s_0])))
    # trajs_sde = []
    if (not hasattr(s_0, '__iter__')):
        s_0 = sp.array([s_0])
    for s in s_0:
        s_pos = sp.array(graph_pos[nodelist[s]])
        # print(nodelist[s], gp_ode_func(s_pos),
        #       sp.sum([sp.array(graph_pos[n2] - graph_pos[n1])
        #               * G[n1][n2]['weight'] for n1, n2 in nx.edges(G, nodelist[s])], axis=0))
        # print(s_pos)

        # SCIPY ODE INTEGRATION for Fluid SS
        print()
        print("fluid integration...")

        traj_fluid += odeint(compare_ss.gp_ode_func, s_pos, t_lin, args=(GPR,))
        if ode_grad is not None:
            traj_ode += odeint(ode_grad, graph_pos_ode[nodelist[s]], t_lin)

        # EULER INTEGRATION for Fluid SS
        # traj_fluid += compare_ss.eulerint(gp_ode_func, s_pos, t_lin)

        # SDEINT Itô SDE integration
        # trajs_sde += [itoint(gp_ode_func, sigma, s_pos, t_lin)
        #               for _ in range(5)]
    traj_fluid /= len(s_0)
    traj_fluid = sp.column_stack((t_lin, traj_fluid))
    if ode_grad is not None:
        traj_ode /= len(s_0)
        traj_ode = sp.column_stack((t_lin, traj_ode))

    # BARRIER SET
    # """
    # pCTMCs
    # SIR --- barrier indices
    """
    barrier_label = '$R \geq N / 10$'
    R_bar = 1 * pop_total / 10
    barrier_indices = [nodelist.index(node)
                       for node in nodelist if node[-1] >= R_bar]
    # """

    # LV --- barrier indices
    # """
    pred_bar = sp.arange((2 * pop_total) // 10, (6 * pop_total) // 10)
    barrier_label = f'$6N/10 > Pred \geq 2N/10$'
    barrier_indices = [nodelist.index(node)
                       for node in nodelist if (node[-1] in pred_bar)]
    # """

    # PEPA --- barrier indices
    # barrier_indices = sp.arange(3 * N // 4, N)

    # Embedded barrier points
    # barrier_pos = [graph_pos[nodelist[idx]] for idx in barrier_indices]
    graph_pos_arr = sp.array([graph_pos[i] for i in nodelist])
    graph_pos_ode_arr = sp.array([graph_pos_ode[i] for i in nodelist])
    # """

    # PLOTTING DM projection
    # PROJECTING DISCRETE P_ss TO CONTINUOUS DOMAIN
    p_ss, _ = simulate_ss.probability_ss(trajs, N=len(nodelist), t_lin=t_lin)
    p_R_ss = sp.zeros((ticks, n_dim))
    p_R_ss = p_ss @ graph_pos_arr
    _, _ = compare_ss.plot_DM_projection(t_lin, graph_pos, G,
                                         traj_fluid[:, 1:], p_R_ss)

    # Finding FPTs for CTMC and fluid trajectories
    fpt_ctmc_res = fpt_ctmc(trajs=trajs, barrier=barrier_indices)
    fpt_fluid_res = fpt_fluid(traj=traj_fluid,
                              barrier=barrier_indices,
                              graph_pos=graph_pos_arr)
    if ode_grad is not None:
        fpt_ode_res = fpt_fluid(traj=traj_ode,
                                barrier=barrier_indices,
                                graph_pos=graph_pos_ode_arr)
    else:
        fpt_ode_res = None
    return fpt_ctmc_res, fpt_fluid_res, fpt_ode_res, sp.array(nodelist[s]), barrier_label
    # """


def fpt_plot_scaled_pCTMC(pCTMC_sys, pop_totals=None):
    if pop_totals is None:
        pop_totals = sp.array([3, 4, 5, 10]) * 10

    # Simulations for each population size
    fpt_ctmc_res = []
    fpt_fluid_res = []
    fpt_ode_res = [0]  # should be same for all N
    for pop in pop_totals:
        print('\n##############')
        print(f'Estimating FPT for pCTMC, population N={pop}')
        print('##############\n')

        fpt_ctmc_r, fpt_fluid_r, fpt_ode_r, s_0, barrier_label = fpt_compare_pCTMC(
            pCTMC_sys, pop_total=pop)
        fpt_ctmc_res += [fpt_ctmc_r]
        fpt_fluid_res += [fpt_fluid_r]
        if fpt_ode_r is not None:
            fpt_ode_res[0] = fpt_ode_r
        print('s_0:', s_0)

    # PLOTTING
    import matplotlib.cm as cm
    norm = mpl.colors.Normalize(vmin=sp.amin(pop_totals),
                                vmax=sp.amax(pop_totals))
    fluid_cm = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(name='copper_r'))
    ctmc_cm = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(name='winter_r'))

    # Get system name for title
    system_label = pCTMC_sys()[-1]

    fig, ax = plt.subplots()

    # Plot cumulative histograms
    n_bins = 1000

    for i, pop in enumerate(pop_totals):
        # empirical FPT from ctmc SSA
        n, bins, patches = ax.hist(fpt_ctmc_res[i], n_bins, density=True,
                                   histtype='step', cumulative=True,
                                   label=f'Empirical FPT, $N={pop}$',
                                   color=ctmc_cm.to_rgba(pop))
        # removing right edge
        patches[0].set_xy(patches[0].get_xy()[:-1])

    for i, pop in enumerate(pop_totals):
        # Vertical line showing the FPT of fluid trajectory
        n, bins, patches = ax.hist(fpt_fluid_res[i], n_bins, density=True,
                                   histtype='step', cumulative=True,
                                   label=f'Fluid FPT, $N={pop}$',
                                   color=fluid_cm.to_rgba(pop))
        # removing right edge
        patches[0].set_xy(patches[0].get_xy()[:-1])

    # Vertical line showing the FPT of the standard ODE trajectory
    if fpt_ode_r is not None:
        n, bins, patches = ax.hist(fpt_ode_res[0], n_bins, density=True,
                                   histtype='step', cumulative=True,
                                   label='ODE FPT, any $N$',
                                   color='red')
        # removing right edge
        patches[0].set_xy(patches[0].get_xy()[:-1])

    ax.legend(loc=4)
    ax.set_xlabel('time (s)')
    ax.set_ylabel('CDF')
    # SIR
    if system_label == 'SIRS':
        frac = s_0 / sp.sum(s_0)
        frac = sp.array2string(s_0 / sp.sum(s_0), separator=', ')
        ax.set_title(f'FPT for {barrier_label}\n'
                     f'({system_label}, $s_0 = N{frac}$)')
    # LV
    elif system_label == 'Lotka-Volterra':
        frac = sp.array2string(s_0 / sp.sum(s_0), separator=', ')
        ax.set_title(f'FPT for {barrier_label}\n'
                     f'({system_label}, $s_0 = N{frac}$)')
        ax.set_xlim(right=3.0)
    plt.show()


if __name__ == '__main__':
    # import sp_SIR
    # sys = sp_SIR.sp_SIR

    # import Lotka_Volterra
    # sys = Lotka_Volterra.sp2_LV

    import sp_CTMC_perturbed
    # sys = sp_CTMC_perturbed.sp2_LV_perturbed
    sys = sp_CTMC_perturbed.sp2_LV_perturbed_reduced

    fpt_plot_scaled_pCTMC(sys, pop_totals=[30, 40, 50])
