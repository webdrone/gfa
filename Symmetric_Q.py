import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt


def symmetric_LV(N=100):
    import Lotka_Volterra

    G = Lotka_Volterra.sp2_LV(N=30)

    # symmetric part
    nodelist = G.nodes()
    Q = sp.asarray(nx.adjacency_matrix(
        G, nodelist=nodelist, weight='weight').todense())
    Q_sym = 1 / 2 * (Q + Q.T)
    sp.fill_diagonal(Q_sym, 0)
    # sp.fill_diagonal(Q_sym, -sp.sum(Q_sym, axis=1))
    G = nx.DiGraph(Q_sym)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


if __name__ == '__main__':
    G = symmetric_LV(N=20)

    # Saving adjacency matrix of undirected pCTMC state graph
    G_undir = G.to_undirected()
    nodelist = G.nodes()

    A = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())
    sp.savetxt('sp2_symmetric.csv', A, fmt='%d', delimiter=',')

    Poynting, GPR, Poynting_pred = embed_ss.embed_graph(
        G, proj_dim=2, plot_edges=True)
    plt.title("State projection on continuous domain and imputed vector field.")
    plt.show()
