import scipy as sp

# sklearn GPs
# '''
from sklearn.gaussian_process import GaussianProcessRegressor
import sklearn.gaussian_process.kernels as kernels

%matplotlib
# import matplotlib as mpl
import matplotlib.pyplot as plt

kern_rbf = kernels.RBF(length_scale=1)
kern_ampl = kernels.ConstantKernel(constant_value=1)
kern_white_noise = kernels.WhiteKernel(noise_level=1e-3)
kern = kernels.Sum(kernels.Product(kern_ampl, kern_rbf), kern_white_noise)
gp = GaussianProcessRegressor(kernel=kern, alpha=0., optimizer=None)

X = sp.rand(10).reshape(-1, 1)
y = sp.rand(10).reshape(-1, 1)

gp.fit(X, y)

t_lin = sp.linspace(-4.5, 5.5, 1000)
X_pred = t_lin.reshape(-1, 1)

y_pred = gp.predict(X_pred)

plt.plot(t_lin, y_pred)
plt.scatter(X, y, c='r')

# Log scale
kern_log = kernels.Product(kern_ampl, kern_rbf)
gp_log = GaussianProcessRegressor(kernel=kern_log,
                                  alpha=1e-1,
                                  # optimizer=None
                                  )
y_log = sp.log(y) + 5
gp_log.fit(X, y_log)
y_log_pred = gp_log.predict(X_pred)

plt.figure()
plt.title('Log scale GP fit')
plt.plot(t_lin, y_log_pred)
plt.scatter(X, y_log, c='r')

plt.figure()
plt.title('Transforming log GP fit to original space')
plt.plot(t_lin, sp.exp(y_log_pred - 5))
plt.scatter(X, y, c='r')
