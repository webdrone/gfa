import scipy as sp
from scipy import sparse
from scipy import linalg as la
from scipy.integrate import odeint
from scipy.sparse.linalg import expm_multiply

import sdeint
from sdeint import itoint

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import networkx as nx

import embed_ss
import simulate_ss


# Loading PEPA model
def load_PEPA(name=None):
    if name is None:
        # LOADING PEPA MODEL
        # pepa_fname = 'PEPA_test.generator'
        # pepa_fname = 'PEPA_TOMP_4-4_less.generator'
        # pepa_fname = 'PEPA_TOMP_4-4_2560.generator'
        # pepa_fname = 'PEPA_TOMP_2-2-1_416.generator'
        pepa_fname = 'PEPA_WS_3-3-2-2_1376.generator'
        # pepa_fname = 'PEPA_TOMP_3-3-3_10752.generator'
        # pepa_fname = 'PEPA_TOMP_4-3-3_23552.generator'
    else:
        pepa_fname = name
    # """
    # LOADING Q MATRIX
    gen = sp.loadtxt(pepa_fname, delimiter=',')

    row = (gen[:, 0] - 1).astype(int)
    col = (gen[:, 1] - 1).astype(int)
    data = gen[:, -1]
    N = max(sp.hstack((row, col))) + 1
    print(N)
    Q = sparse.csr_matrix((data, (row, col)),
                          shape=(N, N)).toarray()
    sp.fill_diagonal(Q, 0)

    G = nx.from_numpy_matrix(Q, create_using=nx.DiGraph())

    import sp_CTMC_coffin
    # G = sp_CTMC_coffin.build_subset(G, root=100, radius=2)
    # G = sp_CTMC_coffin.build_coffin(G, root=500, radius=4)

    nodelist = list(G.nodes())
    N = len(nodelist)
    Q = sp.asarray(nx.adjacency_matrix(G, nodelist=nodelist).todense())
    print(Q.shape)
    # """
    return G, Q, nodelist


# Loading pCTMC model
def load_pCTMC(pop_total=50):
    import sp_CTMC
    # G, ode_grad, system_label = sp_CTMC.sp2_birth(N=pop_total)
    # G = sp_CTMC.sp2_react(N=25)
    # G = sp_CTMC.sp2_2react(N=25)
    G, ode_grad, system_label = sp_CTMC.sp2_gene_switch(N=pop_total)

    # import Lotka_Volterra
    # G, ode_grad, system_label = Lotka_Volterra.sp2_LV(N=pop_total)

    # import sp_SIR
    # G, ode_grad, system_label = sp_SIR.sp_SIR(N=pop_total)

    # import sp_CTMC_perturbed
    # G, ode_grad, system_label = sp_CTMC_perturbed.sp2_birth(N=pop_total)
    # G, ode_grad, system_label = sp_CTMC_perturbed.sp2_LV_perturbed(N=pop_total)
    # G, ode_grad, system_label = sp_CTMC_perturbed.sp2_LV_reduced(N=pop_total)
    # G, ode_grad, system_label = sp_CTMC_perturbed.sp2_LV_perturbed_reduced(N=pop_total)

    # import sp_CTMC_coffin
    # G = sp_CTMC_coffin.build_subset_sp2(G, root=(9, 21), radius=8)
    # G = sp_CTMC_coffin.build_coffin_sp2(G, root=(9, 21), radius=8)

    # import Symmetric_Q
    # G, ode_grad, system_label = Symmetric_Q.symmetric_LV(N=pop_total)

    nodelist = list(G.nodes())
    # Q = sp.array(nx.convert_matrix.to_numpy_matrix(G, nodelist=nodelist))
    Q = sp.asarray(nx.adjacency_matrix(G, nodelist=nodelist).todense())
    sp.fill_diagonal(Q, -sp.sum(Q - sp.diag(sp.diag(Q)), axis=1))
    # """
    return G, Q, nodelist, system_label


# GP for v field
def gp_ode_func(x, t0, GPR):
    # GPflow GPR
    # v_pred, var = GPR.predict_y(sp.array([x]))
    x = sp.atleast_2d(x)
    # sklearn GPR
    v_pred = GPR.predict(x)
    v_pred = sp.squeeze(v_pred)
    return v_pred


# LA for v field
def la_ode_func(x, t0, X, X_inv, Q):
    v_pred = X.T @ Q.T @ X_inv.T @ x
    return v_pred


# Analytical drift vector field (if it exists)
def fluid_ode_func(x, t0=None):
    v_pred = sp.array(x)
    v_pred = x * (2 - x)
    return v_pred


# Itô SDE INTEGRATION
def sigma(x, t0=None):
    d_max = sp.inf
    for k, v in Diffusion_matrix_dict.items():
        if sp.linalg.norm(v[0] - x) < d_max:
            std = v[1]
    std = sp.linalg.sqrtm(std)
    return std


"""
# EULER INTEGRATION for Fluid SS
def eulerint(grad_func, t, t_lin):
    subticks = 1
    traj_fluid = []
    s_pos = sp.array(t)
    dt = t_lin[1] / subticks
    print(dt)
    t = 0
    for t_end in t_lin:
        traj_fluid += [sp.array(s_pos)]
        while t < t_end:
            v = grad_func(s_pos, t)
            # print(v)
            s_pos = sp.array(s_pos + (dt * v))
            t += dt
    traj_fluid = sp.array(traj_fluid)
    return traj_fluid
# """


def plot_DM_projection(t_lin, graph_pos, G, traj_fluid, p_R_ss,
                       Poynting_vec_field=None,
                       system_label='',
                       nodelist=None,
                       s_0=None):

    n_dim = sp.shape(traj_fluid)[-1]

    # PLOTTING TRAJECTORIES
    plot_context = True  # plot states, flows
    plot_nodes = False  # plot states
    plot_edges = True  # plot state connexions

    if n_dim == 3:
        # """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(p_R_ss[:, 0], p_R_ss[:, 1], p_R_ss[:, 2],
                color='blue', label='SSA average')
        ax.plot(traj_fluid[:, 0], traj_fluid[:, 1], traj_fluid[:, 2],
                color='red', label='Fluid estimate')

        if plot_context:
            if plot_nodes:
                # nodes
                graph_pos_val = sp.array(list(graph_pos.values()))
                ax.scatter(graph_pos_val[:, 0], graph_pos_val[:, 1],
                           graph_pos_val[:, 2],
                           s=20,
                           marker='o',
                           c='r')
            if plot_edges:
                edges = G.edges()
                for ed in edges:
                    ax.plot(*zip(graph_pos[ed[0]], graph_pos[ed[1]]),
                            color='grey', linewidth=0.5)

            # Vector field
            # Removing very large arrows that clutter graph
            # Poynting_vec_field_pred = sp.hstack(
            #     (Poynting_vec_field_pred[:, :3],
            #      Poynting_vec_field_pred[:, n_dim:n_dim + 3]))
            # P_idx = la.norm(Poynting_vec_field_pred[:, 3:],
            #                 axis=-1) < 2.0 * sp.mean(
            #     la.norm(Poynting_vec_field_pred[:, 3:], axis=-1))
            # Poynting_vec_field_pred = Poynting_vec_field_pred[P_idx]
            # P_idx = la.norm(Poynting_vec_field_pred[:, 3:],
            #                 axis=-1) > 0.0 * sp.mean(
            #     la.norm(Poynting_vec_field_pred[:, 3:], axis=-1))
            # Poynting_vec_field_pred = Poynting_vec_field_pred[P_idx]

            # sc = 0.1
            # sc = 15 * sc / len(Poynting_vec_field_pred[:, 3:]) ** 0.5

            # Poynting_vec_field_pred[:, 3:] *= sc
            # ax.quiver(*Poynting_vec_field_pred.T)
        ax.set_xlabel('DM dimension: $d=1$')
        ax.set_ylabel('DM dimension: $d=2$')
        ax.set_ylabel('DM dimension: $d=3$')
        ax.legend()
        if None not in (nodelist, s_0):
            ax.set_title('Trajectories on DM manifold'
                         f'({system_label}, $s_0={nodelist[s_0[0]]}$')
        # """
    if n_dim >= 3:
        # """
        n_dim = min(20, n_dim)

        # fig, axarr = plt.subplots(n_dim, sharex=True)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        linstys = ['-', '--', ':']

        for d in range(n_dim):
            # for pos in sp.array(list(graph_pos.values())):
            # ax.plot(t_lin, [sp.array(list(graph_pos.values()))[
            #     :, d]] * len(t_lin), color='grey', linewidth=0.5)

            """
            # plotting CTMC trajectories (only 1 legend)
            ax[d].plot(t_lin, trajs_ctmc[0][:, d],
                          color='cyan', label='CTMC')
            for tr in trajs_ctmc[1:]:
                ax[d].plot(t_lin, tr[:, d],
                              color='cyan', label=None)

            # plotting SDE trajectories (only 1 legend)
            ax[d].plot(t_lin, trajs_sde[0][:, d],
                          color='tomato', label='Itô SDE int')
            for tr in trajs_sde[1:]:
                ax[d].plot(t_lin, tr[:, d],
                              color='tomato', label=None)
            # """

            ax.plot(t_lin, p_R_ss[:, d],
                    color='blue', linestyle=linstys[d % len(linstys)],
                    label=f'SSA average, $d={d}$')
        for d in range(n_dim):
            ax.plot(t_lin, traj_fluid[:, d],
                    color='red', linestyle=linstys[d % len(linstys)],
                    label=f'Fluid estimate, $d={d}$')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Position along dimension $d$ of DM')
        ax.legend()
        if None not in (nodelist, s_0):
            ax.set_title('Evolution along Diffusion Map dimensions\n'
                         f'({system_label}, $s_0={nodelist[s_0[0]]}$)')
        # """
    else:
        fig = plt.figure()
        ax = fig.add_subplot(111)

        graph_pos_val = sp.array(list(graph_pos.values()))

        if plot_nodes:
            # nodes
            ax.scatter(graph_pos_val[:, 0], graph_pos_val[:, 1],
                       s=20,
                       marker='o',
                       c='r')

        if plot_edges:
            # edges
            edges = G.edges()
            for ed in edges:
                ax.plot(*sp.column_stack((graph_pos[ed[0]], graph_pos[ed[1]])),
                        color='grey', linewidth=0.4)

            # Drift vector field at state positions
            if Poynting_vec_field is not None:
                ax.quiver(*Poynting_vec_field.T)
                # ax.quiver(*Poynting_vec_field_pred.T, scale=1)

        if n_dim == 1:
            p_R_ss = sp.column_stack((p_R_ss, sp.zeros(len(p_R_ss))))
            traj_fluid = sp.column_stack(
                (traj_fluid, sp.ones(len(traj_fluid))))
            graph_pos_val = sp.column_stack(
                (graph_pos_val, sp.zeros(len(graph_pos_val))))

        """
        # plotting CTMC trajectories (only 1 legend)
        ax.plot(trajs_ctmc[0][:, 0], trajs_ctmc[0][:, 1],
                color='cyan', label='CTMC')
        for tr in trajs_ctmc[1:]:
            ax.plot(tr[:, 0], tr[:, 1],
                    color='cyan', label=None)

        # plotting SDE trajectories (only 1 legend)
        ax.plot(trajs_sde[0][:, 0], trajs_sde[0][:, 1],
                color='tomato', label='Itô SDE int')
        for tr in trajs_sde[1:]:
            ax.plot(tr[:, 0], tr[:, 1],
                    color='tomato', label=None)
        # """

        # plotting CTMC mean
        ax.plot(p_R_ss[:, 0], p_R_ss[:, 1],
                color='blue', label='SSA average')

        # plotting fluid limit
        ax.plot(traj_fluid[:, 0], traj_fluid[:, 1],
                color='red', label='Fluid estimate')

        ax.set_xlabel('DM dimension: $d=1$')
        ax.set_ylabel('DM dimension: $d=2$')
        ax.legend()
        if None not in (nodelist, s_0):
            ax.set_title('Trajectories on DM manifold\n'
                         f'({system_label}, $s_0={nodelist[s_0[0]]}$).')

        fig = plt.figure()
        ax = fig.add_subplot(111)
        linstys = ['-', '--', ':']
        for d in range(n_dim):
            ax.plot(t_lin, p_R_ss[:, d],
                    color='blue', linestyle=linstys[d],
                    label=f'SSA average, $d={d}$')
        for d in range(n_dim):
            ax.plot(t_lin, traj_fluid[:, d],
                    color='red', linestyle=linstys[d],
                    label=f'Fluid estimate, $d={d}$')
        ax.set_xlabel('time (s)')
        ax.set_ylabel('Position along dimension $d$ of DM')
        ax.legend()
        if None not in (nodelist, s_0):
            ax.set_title('Evolution along Diffusion Map dimensions\n'
                         f'({system_label}, $s_0={nodelist[s_0[0]]}$).')
    return fig, ax


if __name__ == '__main__':
    # """
    # LOADING CTMC
    pop_total = 50
    # G, Q, nodelist = load_PEPA()
    G, Q, nodelist, system_label = load_pCTMC(pop_total=pop_total)
    nodelist_subset = list(nodelist)
    subset_idx = None

    # subsets
    """
    import sp_CTMC_coffin
    G = sp_CTMC_coffin.build_subset_sp2(G, root=(9, 21), radius=8)
    # G = sp_CTMC_coffin.build_coffin_sp2(G, root=(9, 21), radius=8)
    nodelist_subset = list(G.nodes())
    subset_idx = [nodelist.index(n) for n in nodelist_subset if n in nodelist]
    # """

    # Embedding in \mathbb{R}^{n_dim}
    n_dim = 2
    N = len(nodelist_subset)
    # """

    print()
    print("Embedding graph and GP for gradient field...")

    graph_pos, GPR, Poynting_vec_field, Diffusion_matrix_dict = embed_ss.embed_graph(
        G, proj_dim=n_dim, plot=False, nodelist=nodelist_subset)
    # print(graph_pos)
    # print(nodelist)
    sp.fill_diagonal(Q, -sp.sum(Q - sp.diag(sp.diag(Q)), axis=1))
    # print(Q)
    # print(nodelist_subset[0], nodelist_subset[-4])

    # DRAWING MULTIPLE TRAJECTORIES (DISCRETE SS)
    n_traj = 1 * 10 ** 3
    t_end = 100
    # sp.array([sp.arange(10)] * 100).flatten()

    # SIRS s_0
    # pop_total = sp.sum(nodelist[0])
    # S_frac = 8 * (pop_total) // 10
    # I_frac = 2 * (pop_total) // 10
    # R_frac = pop_total - (S_frac + I_frac)

    # s_0 = nodelist.index((S_frac, I_frac, R_frac))

    # LV s_0
    # prey_num = pop_total * 3 // 10
    # pred_num = pop_total - prey_num
    # s_0 = nodelist.index((prey_num, pred_num))

    # Gene switch s_0
    s_0 = nodelist.index((10, 0))
    print('initial state', nodelist[s_0])

    print()
    print("Simulating from Q, n_traj =", n_traj)

    trajs = simulate_ss.simulate_Q(Q, s_0=s_0, time_end=t_end, n_traj=n_traj)

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME (DISCRETE SS)
    ticks = int(t_end / 0.01)
    t_lin = sp.linspace(0, t_end, ticks)

    if subset_idx is None:
        p_ss, trajs = simulate_ss.probability_ss(trajs, N=N, t_lin=t_lin)
    else:
        p_ss, trajs = simulate_ss.probability_ss_subset(
            trajs, N=N, t_lin=t_lin, subset=subset_idx)
    print("Probability over SS sum, should be 1")
    print(sp.amin(sp.sum(p_ss, axis=1)), sp.amax(sp.sum(p_ss, axis=1)))

    # PROJECTING DISCRETE P_ss TO CONTINUOUS DOMAIN
    # ALGEBRAIC Projection
    """
    # Delta = 1 / 2 * (Q + Q.T)
    # U = la.eig(Delta)[1]
    U = sp.eye(len(nodelist_subset))
    graph_pos = {n: U[i] for i, n in enumerate(nodelist_subset)}
    # """
    Y = sp.array([graph_pos[i] for i in nodelist_subset])
    Y_inv = Y.T  # la.lstsq(Y, sp.eye(len(Y)))[0]

    p_R_ss = sp.zeros((ticks, n_dim))
    p_R_ss = p_ss @ Y

    """
    # PROJECTING DISCRETE TRAJS TO CONTINUOUS DOMAIN
    trajs_ctmc = [sp.zeros((ticks, n_dim))] * n_traj
    for i in range(n_traj):
        trajs_ctmc[i] = sp.array(
            [graph_pos[nodelist[int(s)]] for s in trajs[i]])
    # """

    # FLUID INTEGRATION PATHS
    traj_fluid = sp.zeros((ticks, n_dim))
    trajs_sde = []
    if (not hasattr(s_0, '__iter__')):
        s_0 = sp.array([s_0])
    for s in s_0:
        s_pos = sp.array(graph_pos[nodelist_subset[s]])
        # print(nodelist[s], gp_ode_func(s_pos),
        #       sp.sum([sp.array(graph_pos[n2] - graph_pos[n1])
        #               * G[n1][n2]['weight'] for n1, n2 in nx.edges(G, nodelist[s])], axis=0))
        # print(s_pos)

        # SCIPY ODE INTEGRATION for Fluid SS
        print()
        print("fluid integration...")

        traj_fluid += odeint(gp_ode_func, s_pos, t_lin, args=(GPR,))

        # ALGEBRAIC Projection
        """
        traj_fluid = expm_multiply((Y_inv @ Q @ Y).T, s_pos,
                                   start=0, stop=t_end,
                                   num=ticks, endpoint=True)
        # """

        # EULER INTEGRATION for Fluid SS
        # traj_fluid += eulerint(gp_ode_func, s_pos, t_lin)

        # SDEINT Itô SDE integration
        # trajs_sde += [itoint(gp_ode_func, sigma, s_pos, t_lin)
        #               for _ in range(5)]
    traj_fluid /= len(s_0)

    fig, ax = plot_DM_projection(t_lin, graph_pos, G, traj_fluid, p_R_ss,
                                 # Poynting_vec_field=Poynting_vec_field,
                                 system_label=system_label,
                                 nodelist=nodelist, s_0=s_0)

    plt.show()
    print()
