import scipy as sp
from scipy import linalg as la
import networkx as nx
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# sklearn GPs
# '''
from sklearn.gaussian_process import GaussianProcessRegressor
import sklearn.gaussian_process.kernels as kernels
from sklearn.metrics.pairwise import laplacian_kernel
from sklearn.manifold import MDS
# from sklearn.manifold import SpectralEmbedding
from sklearn.decomposition import PCA as skPCA
from sklearn.preprocessing import normalize

from multi_kernel import GPR_nocurl
from multi_kernel import GPR_nodiv
# '''

# GPflow (TensorFlow GPs)
'''
import gpflow
from gpflow import transforms
from gpflowopt import optim, BayesianOptimizer
from gpflowopt.domain import ContinuousParameter
from gpflowopt.design import LatinHyperCube
from gpflowopt.acquisition import ExpectedImprovement
from gpflowopt.objective import batch_apply
# '''


def PCA(M):
    n_nodes = len(M)
    # Centring
    J = sp.eye(n_nodes) - 1 / n_nodes * (sp.ones((n_nodes, 1)) @
                                         sp.ones((1, n_nodes)))
    M = -0.5 * J @ M @ J

    w, v = la.eigh(M)
    return w, v


def plot_network(G, graph_pos, proj_dim,
                 Poynting_vec_field, Poynting_vec_field_pred, plot_edges):
    # Plotting
    fig = plt.figure()
    ax = fig.add_subplot(111)
    # nx.draw_networkx(G, pos=graph_pos, node_size=20, with_labels=False)

    graph_pos_val = sp.array(list(graph_pos.values()))
    if proj_dim < 3:
        # nodes
        ax.scatter(graph_pos_val[:, 0], graph_pos_val[:, 1],
                   s=20,
                   marker='o',
                   c='r')

        if plot_edges:
            # edges
            edges = G.edges()
            for ed in edges:
                ax.plot(*sp.column_stack((graph_pos[ed[0]], graph_pos[ed[1]])),
                        color='grey', linewidth=0.4)
            # Vector field
            # Removing very large arrows that clutter graph
            # P_idx = la.norm(Poynting_vec_field_pred[:, n_dim:],
            #                 axis=-1) < 1. * sp.mean(
            #     la.norm(Poynting_vec_field_pred[:, n_dim:], axis=-1))
            # Poynting_vec_field_pred = Poynting_vec_field_pred[P_idx]
            # P_idx = la.norm(Poynting_vec_field_pred[:, n_dim:],
            #                 axis=-1) > 0.0 * sp.mean(
            #     la.norm(Poynting_vec_field_pred[:, n_dim:], axis=-1))
            # Poynting_vec_field_pred = Poynting_vec_field_pred[P_idx]

            # sc = 0.51
            # sc = 10 * sc / (len(Poynting_vec_field_pred) ** 0.5)

            # Poynting_vec_field_pred[:, n_dim:] *= sc
            # graph_pos_vec = gp_ode_func(graph_pos_val)
            # Poynting_vec_field = sp.hstack((graph_pos_val,
            #                                 graph_pos_vec))
            ax.quiver(*Poynting_vec_field.T)
            # ax.quiver(*Poynting_vec_field_pred.T, scale=1)
        ax.set_xlabel('DM dimension: $d=1$')
        ax.set_ylabel('DM dimension: $d=2$')
        ax.set_title('Diffusion map embedding')

    elif proj_dim == 3:
        ax = fig.gca(projection='3d')               # 3d axes instance

        # nodes
        ax.scatter(graph_pos_val[:, 0], graph_pos_val[:, 1], graph_pos_val[:, 2],
                   s=20,
                   marker='o',
                   c='r')
        # ax.text(*graph_pos[G.nodes()[0]] * 1.05, "(0, 0)")
        # ax.text(*graph_pos[G.nodes()[-1]] * 1.05, "(14, 14)")

        # edges
        if plot_edges:
            edges = G.edges()
            for ed in edges:
                ax.plot(*zip(graph_pos[ed[0]], graph_pos[ed[1]]),
                        color='grey')

        ax.quiver(*Poynting_vec_field.T, length=5)
        ax.quiver(*Poynting_vec_field_pred.T, length=0.5)

        ax.set_xlabel('DM dimension: $d=1$')
        ax.set_ylabel('DM dimension: $d=2$')
        ax.set_zlabel('DM dimension: $d=3$')
        ax.set_title('Diffusion map embedding')
        ax.set_zlim(min(graph_pos_val[:, 2]) - 1, max(graph_pos_val[:, 2]) + 1)


def embed_graph(G, proj_dim=2,
                nodelist=None,
                plot=True,
                plot_edges=False,
                plot_eigenvalues=True):
    # ======================================
    # PROJECTING GRAPH ON CONTINUOUS DOMAIN
    # POYNTING VECTOR FIELD AT NODES
    # ======================================
    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    # graph_pos = nx.spectral_layout(G, dim=proj_dim, weight=None)
    # graph_pos = dict(zip(G.nodes(), sp.array(G.nodes())))
    coords = sp.eye(n_nodes)
    if nodelist is None:
        nodelist = G.nodes()
    graph_pos = dict(zip(nodelist, coords))

    # """
    # Diffusion maps
    # G_undir = G.to_undirected()

    Q = sp.asarray(nx.adjacency_matrix(
        G, nodelist=nodelist, weight='weight').todense())
    sp.fill_diagonal(Q, -sp.sum(Q - sp.diag(sp.diag(Q)), axis=1))
    print('Max, min on Q diagonal (should be -ve):', sp.amax(sp.diag(Q)),
          sp.amin(sp.diag(Q)))

    # uniformisation to find W
    eps = min(1. / sp.amax(-sp.diag(Q)), 1. / sp.amax((-sp.diag(Q)) ** 4))
    P = sp.eye(len(Q)) + Q * eps

    W = P
    W = (sp.diag(1 / sp.diag(W))) @ W
    print('Max, min on W diagonal (should be 1):', sp.amax(sp.diag(W)),
          sp.amin(sp.diag(W)))

    # taking the symmetric kernel
    # s_eps = \int_t k_eps(x, t) k_eps(y, t) dt
    # S = (W + W^T) / 2

    def H(alpha, W, sym=False):
        if sym:
            # S = W @ W.T
            S = (W + W.T) / 2
            # S = (Q + Q.T) / 2
            # sp.fill_diagonal(S, -sp.sum(S - sp.diag(sp.diag(S)), axis=1))
            # return S
        else:
            S = W
            # return Q
        Ps_inv = sp.diag(sp.sum(S, axis=1) ** -alpha)
        V = Ps_inv @ S @ Ps_inv
        Ds_inv = sp.diag(sp.sum(V, axis=1) ** -1)
        if sym:
            # H_ss = (Ds_inv ** (1 / 2) @ V @ Ds_inv ** (1 / 2) - sp.eye(len(V))) / eps
            H_ss = (Ds_inv ** (1 / 2) @ V @ Ds_inv ** (1 / 2))
            # H_ss = eps * S + sp.eye(len(Q))
        else:
            H_ss = Ds_inv @ V
        return H_ss, Ds_inv

    alpha = 0
    H_ss, Dinv_ss = H(alpha, W, sym=True)
    H0_ss, Dinv0_ss = H(0, W, sym=True)
    H1_ss, Dinv1_ss = H(1, W, sym=True)

    # S_diff = Ts - Ts.T
    # print('checking symmetry:', sp.sum(S_diff))
    # print(S_diff[sp.absolute(S_diff) > 1e-3])

    eigval, eigvec = la.eigh(H1_ss)
    idx = eigval.argsort()[::-1]
    # eigvec = Dinv1_ss ** (1 / 2) @ eigvec
    eigvec = eigvec[:, idx]
    eigval = eigval[idx]

    if plot_eigenvalues:
        plt.figure()
        plt.plot(sp.arange(len(eigval)), eigval, marker='o')
        plt.title('Eigenvalues')

    print('Eigenvalues (first 10 and shape)')
    print(eigval[:10])
    print(eigvec.shape)

    # t = eps

    # F = normalize(eigvec, axis=0)
    F = eigvec[:, 1:(proj_dim + 1)]
    # F = eigvec[:, 1:proj_dim + 1] @ sp.diag(eigval[1:proj_dim + 1])

    print("normality check:", F[:, 0]@F[:, 0])
    # F = eigvec[:, 0:proj_dim]
    # F *= 100
    # Leigval = sp.diag(eigval[1:proj_dim + 1].real)

    # print('Standard deviation of states position for each dimension:',
    #       sp.std(F, axis=0))

    graph_pos = dict(zip(nodelist, F))

    # Node density
    # eigval, eigvec = la.eigh(H_ss)
    # idx = eigval.argsort()[::-1]
    pid = eigvec[:, 0]
    pid = pid / sp.sum(pid)

    print("Density (max, min)")
    print(sp.amax(pid), sp.amin(pid))

    H0_aa, Dinv0_aa = H(0, W)
    R = (H0_aa - H0_ss)
    print('max R:', sp.amax(R @ F))
    # print(pid)
    # print("P == H0_aa:", sp.sum(Q == H0_aa) / sp.product(P.shape))

    R_adv = (H0_aa - H1_ss) @ F
    # print(pid)
    print('Max, min of R_adv:', sp.amax(R_adv), sp.amin(R_adv))

    # Estimating vectors at state positions (continuous domain coords)
    Poynting_vec_dict = dict(zip(nodelist, sp.hstack((F, R_adv))))

    Poynting_vec_field = sp.array(list(Poynting_vec_dict.values()))
    # """

    """
    # Structure Preserving Embedding (SPE)
    G_undir = G.to_undirected()
    W = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())

    from pymatbridge import Matlab
    mlab = Matlab()
    mlab.start()
    mlab.run_code("cd SPE")
    res = mlab.run_func("spe.m", W, 0.99, proj_dim, 0)
    Yspe = res['result']
    mlab.stop()
    T = Yspe[:proj_dim].T
    graph_pos = dict(zip(nodelist, T))

    print("Embedding done.")
    # """

    """
    # Laplacian eigenmaps
    G_undir = G.to_undirected()

    Q = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())
    W = Q

    D = sp.diag(sp.sum(W, axis=1))
    L = D - W
    eigval, eigvec = la.eigh(L, D)
    idx = eigval.argsort()
    eigvec = eigvec[:, idx]
    eigval = eigval[idx]

    if plot_eigenvalues:
        plt.figure()
        plt.plot(sp.arange(len(eigval)), eigval, marker='o')
        plt.title('Eigenvalues')

    # print(eigval)
    print(eigvec.shape)

    T = eigvec[:, 1:proj_dim + 1]
    # T = eigvec[:, 0:proj_dim]
    # T *= 100

    print('Standard deviation of states position for each dimension:',
          sp.std(T, axis=0))

    graph_pos = dict(zip(nodelist, T))
    # """

    """
    # MDS METHOD
    G_undir = G.to_undirected()
    L = nx.laplacian_matrix(G_undir).todense()
    M = nx.floyd_warshall_numpy(G_undir, weight='None')

    # M = nx.convert_matrix.to_numpy_matrix(G_undir)
    # M = -M + 2 - 2 * sp.eye(len(M))

    print(M[1, 1])
    skemb = MDS(n_components=proj_dim, metric=True,
                dissimilarity='precomputed', eps=1e-10)
    # skemb = SpectralEmbedding(n_components=proj_dim, n_neighbors=1)
    # skemb = skPCA(n_components=proj_dim)
    # L = M = nx.convert_matrix.to_numpy_matrix(G_undir)
    T = skemb.fit_transform(M)

    # Rotate the data
    clf = skPCA(n_components=proj_dim)
    T = clf.fit_transform(T)
    print(T.shape)

    # """

    # """
    # Estimating vectors at state positions (continuous domain coords)
    Poynting_vec_dict = dict(graph_pos)
    for k, v in Poynting_vec_dict.items():
        Poynting_vec_dict[k] = sp.hstack((v, sp.zeros(proj_dim)))

    for n1, n2 in nx.edges(G):
        direct = sp.array(graph_pos[n2] - graph_pos[n1])
        Poynting_vec_dict[n1] += sp.hstack((sp.zeros(proj_dim),
                                            direct *
                                            G[n1][n2]['weight']
                                            ))

    Poynting_vec_field = sp.array(list(Poynting_vec_dict.values()))
    print('Projection and DM discrepancy (max, min):')
    discrepancy = R_adv / sp.array([Poynting_vec_dict[n]
                                    for n in nodelist])[:, proj_dim:]
    print(sp.amax(discrepancy), sp.amin(discrepancy))
    # """

    # """
    # Estimating vectors at state positions (continuous domain coords)
    Diffusion_matrix_dict = dict(graph_pos)
    for k, v in Diffusion_matrix_dict.items():
        Diffusion_matrix_dict[k] = (v, sp.zeros((proj_dim, proj_dim)))

    for n1, n2 in nx.edges(G):
        direct = sp.array(graph_pos[n2] - graph_pos[n1])
        Diffusion_matrix_dict[n1] = (graph_pos[n1],
                                     G[n1][n2]['weight'] * sp.outer(direct, direct) +
                                     Diffusion_matrix_dict[n1][1]
                                     )
    # """

    # ======================================
    # REGRESSION FOR POYNTING VECTOR FIELD
    # ======================================
    print("Training regression...")
    # Creating prediction mesh points
    Poynting_vec_field_pred = None
    """
    ticks = 20
    mins = sp.amin(Poynting_vec_field[:, :proj_dim], axis=0)
    maxs = sp.amax(Poynting_vec_field[:, :proj_dim], axis=0)
    lins = sp.array([sp.linspace(mi, ma, ticks) for mi, ma in zip(mins, maxs)])
    Poynting_mesh = sp.meshgrid(*lins, indexing='ij')
    Poynting_vec_field_pred = sp.array([m.flatten() for m in Poynting_mesh]).T
    # """
    # Poynting_vec_field_pred = sp.ones((2, proj_dim))

    # -------------------------
    # Sklearn GPs
    model = None
    # """
    # GPR
    kern_rbf = kernels.RBF(length_scale=1e-1 * sp.ones(proj_dim),
                           length_scale_bounds=(1e-8, 1e2))
    # kern_lap = kernels.PairwiseKernel(metric=laplacian_kernel, gamma=1)
    kern_ampl = kernels.ConstantKernel(constant_value=1,
                                       constant_value_bounds=(1e-8, 1e3))
    kern = kernels.Product(kern_ampl, kern_rbf)

    # 2. * RBF(length_scale=sp.array([1.] * proj_dim))
    # rbf_kern = None
    GPR = GaussianProcessRegressor(kernel=kern,
                                   alpha=1e-6,
                                   # optimizer=None,
                                   normalize_y=False,
                                   n_restarts_optimizer=3,
                                   )

    # GPR = GPR_nocurl()
    # GPR = GPR_nodiv()

    # Mean prior
    # r_i_vec = Poynting_vec_field[:, :proj_dim]
    # q_i_vec = la.norm(Poynting_vec_field[:, proj_dim:], axis=-1)
    # print(sp.vstack((q_i_vec, q_i_vec)).shape)
    # r_0 = q_i_vec @ r_i_vec / sp.sum(q_i_vec)
    # print("r_0 =", r_0)

    # Removing mean from data
    # print(r_i_vec.shape, r_0.shape)
    # Poynting_vec_field[:, proj_dim:] -= mean_prior(r_i_vec, r_0)

    # print(Poynting_vec_field.shape)
    GPR.fit(Poynting_vec_field[:, :proj_dim],
            Poynting_vec_field[:, proj_dim:])

    # print(GPR.kernel_)
    if Poynting_vec_field_pred is not None:
        Poynting_pred = GPR.predict(Poynting_vec_field_pred)
    model = GPR

    # end of sklearn GPs """

    # -------------------------
    # GPflow
    """
    def create_model(theta):
        var = theta[0]
        lengthsc = theta[1:]

        k = gpflow.kernels.RBF(input_dim=proj_dim, ARD=True,
                               variance=var, lengthscales=lengthsc)
        k.lengthscales.transform = transforms.Exp()
        k.variance.transform = transforms.Exp()

        m = gpflow.gpr.GPR(Poynting_vec_field[:, :proj_dim],
                           Poynting_vec_field[:, proj_dim:], kern=k)
        m.likelihood.variance = 1e-8

        m._needs_recompile = True

        return m

    # OPTIMISATION
    @batch_apply
    def gpflow_opt(theta=None):
        theta = sp.atleast_2d(theta)

        for th in theta:
            try:
                model_tmp = create_model(th)
                neg_ll = - model_tmp.compute_log_likelihood()
                # print(neg_ll)
                return neg_ll
            except Exception:
                print("Exception.")
                pass
        raise RuntimeError("Failed indefinately.")

    def Bayesian_optimize(fx, n_iter=20, domain=None):
        if domain is None:
            domain = ContinuousParameter('variance', -5, 5)
        lhd = LatinHyperCube((2 * domain.size) ** 2, domain)
        X = lhd.generate()
        X = sp.vstack((X, sp.ones(domain.size)))
        Y = fx(X)
        print(X.shape, Y.shape, domain.size)
        # Y = sp.atleast_2d(Y)

        k_opt = gpflow.kernels.Matern52(domain.size, ARD=False)
        # k_opt = gpflow.kernels.RBF(input_dim=domain.size, ARD=False)

        m = gpflow.gpr.GPR(X, Y, kern=k_opt)
        # m.optimize()

        # Now create the Bayesian Optimizer
        ei = ExpectedImprovement(m)
        opt = optim.StagedOptimizer([optim.MCOptimizer(domain, 100),
                                     optim.SciPyOptimizer(domain)])
        optimizer = BayesianOptimizer(domain, ei, optimizer=opt)

        # Run the Bayesian optimization
        print("Latin Hypercube done... optimising.")
        with optimizer.silent():
            r = optimizer.optimize(fx, n_iter=n_iter)
        return r

    # Non-optimised model
    model = create_model(theta=sp.ones(proj_dim + 1))
    print(model)
    print("NLL", - model.compute_log_likelihood())

    # model.optimize(n_restarts=20)

    # Bayesian optimisation of hyperparameters (var and lengthscale)
    dom = ContinuousParameter('lvariance', 1e-3, 1e4) + \
        sp.sum([ContinuousParameter('llength{0}'.format(i), 1e-2, 1e1)
                for i in range(proj_dim)])
    print(dom)
    print()
    #
    Bayes_opt = Bayesian_optimize(gpflow_opt, n_iter=25, domain=dom)
    theta_opt = Bayes_opt['x'][0]

    model = create_model(theta=theta_opt)

    print("Optimum theta:")
    print(theta_opt)
    print("Optimum NLL", - model.compute_log_likelihood())

    # predicting on state arrows as well
    Poynting_vec_field_pred = sp.vstack((Poynting_vec_field_pred,
                                         Poynting_vec_field[:, :proj_dim]))
    Poynting_pred, var = model.predict_y(Poynting_vec_field_pred)
    # end of GPflow """

    if model is not None and Poynting_vec_field_pred is not None:
        Poynting_vec_field_pred = sp.hstack(
            (Poynting_vec_field_pred, Poynting_pred))

    # -------------------------
    # Plotting
    if plot:
        plot_network(G, graph_pos, proj_dim,
                     Poynting_vec_field, Poynting_vec_field_pred,
                     plot_edges)

    return graph_pos, model, Poynting_vec_field, Diffusion_matrix_dict
