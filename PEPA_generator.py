import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt

N = 15

"""
================
GRID AUTO-GRAPH
================
population = [N, N]
G = nx.grid_graph(dim=population)
G = nx.DiGraph(G)

print(nx.adjacency_matrix(G).getnnz())
print(nx.adjacency_matrix(G))

graph_pos = nx.spectral_layout(G)
edge_labels = {(k1, k2): v for k1, k2, v in G.edges(data='weight')}
nx.draw_networkx(G, pos=graph_pos, edge_labels=edge_labels)

"""

# ============================
# MANUALLY CONSTRUCTING GRAPH
# ============================

# gen = sp.loadtxt('PEPA_test.generator', delimiter=',')
# gen = sp.loadtxt('PEPA_TOMP_4-4_less.generator', delimiter=',')
# gen = sp.loadtxt('PEPA_TOMP_4-4_2560.generator', delimiter=',')
gen = sp.loadtxt('PEPA_TOMP_2-2-1_416.generator', delimiter=',')
# gen = sp.loadtxt('PEPA_WS_3-3-2-2_1376.generator', delimiter=',')
# gen = sp.loadtxt('PEPA_TOMP_3-3-3_10752.generator', delimiter=',')
# gen = sp.loadtxt('PEPA_TOMP_4-3-3_23552.generator', delimiter=',')

row = (gen[:, 0] - 1).astype(int)
col = (gen[:, 1] - 1).astype(int)
data = gen[:, -1]
N = max(sp.hstack((row, col))) + 1
print(N)
Q = sp.sparse.csr_matrix((data, (row, col)),
                         shape=(N, N)).toarray()

print(Q.shape)
G = nx.DiGraph(Q)

n_nodes = len(G.nodes())
n_edges = len(G.edges())

print("n_nodes:", n_nodes)
print("edges:", n_edges)

graph_pos, GPR, _, _ = embed_ss.embed_graph(G, proj_dim=2)
plt.title("State projection on continuous domain and imputed vector field.")
plt.show()
