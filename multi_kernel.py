import scipy as sp
from scipy import sparse
from scipy import linalg as la
from scipy.integrate import odeint


class GPR_nocurl:
    def __init__(self, kernel=None):
        if kernel is None:
            self.kern = self.kern_no_curl
        else:
            self.kern = kernel
        self.X = None

    def kern_no_curl_temp(self, x, y):
        d = len(x)
        sigma = 2.3
        sigma_sq = sigma ** 2
        x_dist_sq = (x - y) @ (x - y)
        G = (1 / sigma_sq) * sp.exp(- x_dist_sq / (2 * sigma_sq)) * \
            (sp.eye(d) - (sp.array([x - y]) /
                          sigma @ sp.array([x - y]).T / sigma))
        return G

    def kern_no_curl(self, X):
        N, D = X.shape
        K = sp.zeros((N * D, N * D))
        """
        # Seems to be wrong -- do not use
        for d in range(D):
            k = N * d
            for d_p in range(D):
                k_p = N * d_p
                K[k: k + N, k_p: k_p + N] = self.kern_no_curl_temp(X[:, d],
                                                                   X[:, d_p])
        """
        for n in range(N):
            k = n * D
            for n_p in range(N):
                k_p = D * n_p
                K[k: k + D, k_p: k_p + D] = self.kern_no_curl_temp(X[n],
                                                                   X[n_p])
        # """
        return K

    def gp_c(self, kern, X, y, lam=1e-10):
        N, D = X.shape
        K = kern(X)
        A = K + lam * N * sp.eye(N * D)
        c = la.solve(A, y)  # , assume_a='sym')
        return c

    def gp_pred(self, x_star, X, c):
        N, D = X.shape
        K_star = sp.zeros((D, N * D))
        for n in range(N):
            k = n * D
            K_star[:, k: k + D] = self.kern_no_curl_temp(x_star, X[n])
        f_star = c @ K_star.T
        return f_star

    def fit(self, X, y):
        y = y.flatten()
        self.c = self.gp_c(self.kern, X, y)
        self.X = sp.array(X)

    def predict(self, X_star):
        f_star = sp.zeros(X_star.shape)
        for i in range(len(X_star)):
            f_star[i] = self.gp_pred(X_star[i], self.X, self.c)
        return f_star


class GPR_nodiv:
    def __init__(self, kernel=None):
        if kernel is None:
            self.kern = self.kern_no_div
        else:
            self.kern = kernel
        self.X = None

    def kern_no_div_temp(self, x, y):
        d = len(x)
        sigma = 1.0
        sigma_sq = sigma ** 2
        x_dist_sq = (x - y) @ (x - y)
        G = (1 / sigma_sq) * sp.exp(- x_dist_sq / (2 * sigma_sq)) * \
            ((sp.array([x - y]) / sigma @ sp.array([x - y]).T / sigma) +
             ((d - 1) - x_dist_sq / sigma_sq) * sp.eye(d))
        return G

    def kern_no_div(self, X):
        N, D = X.shape
        K = sp.zeros((N * D, N * D))
        """
        # Seems to be wrong -- do not use
        for d in range(D):
            k = N * d
            for d_p in range(D):
                k_p = N * d_p
                K[k: k + N, k_p: k_p + N] = self.kern_no_div_temp(X[:, d],
                                                                   X[:, d_p])
        """
        for n in range(N):
            k = n * D
            for n_p in range(N):
                k_p = D * n_p
                K[k: k + D, k_p: k_p + D] = self.kern_no_div_temp(X[n],
                                                                   X[n_p])
        # """
        return K

    def gp_c(self, kern, X, y, lam=1e-5):
        N, D = X.shape
        K = kern(X)
        A = K + lam * N * sp.eye(N * D)
        c = la.solve(A, y, assume_a='sym')
        return c

    def gp_pred(self, x_star, X, c):
        N, D = X.shape
        K_star = sp.zeros((D, N * D))
        for n in range(N):
            k = n * D
            K_star[:, k: k + D] = self.kern_no_div_temp(x_star, X[n])
        f_star = c @ K_star.T
        return f_star

    def fit(self, X, y):
        y = y.flatten()
        self.c = self.gp_c(self.kern, X, y)
        self.X = sp.array(X)

    def predict(self, X_star):
        f_star = sp.zeros(X_star.shape)
        for i in range(len(X_star)):
            f_star[i] = self.gp_pred(X_star[i], self.X, self.c)
        return f_star


if __name__ == '__main__':
    print('hi')
