import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt

"""
================
GRID AUTO-GRAPH
================
population = [N, N]
G = nx.grid_graph(dim=population)
G = nx.DiGraph(G)

print(nx.adjacency_matrix(G).getnnz())
print(nx.adjacency_matrix(G))

graph_pos = nx.spectral_layout(G)
edge_labels = {(k1, k2): v for k1, k2, v in G.edges(data='weight')}
nx.draw_networkx(G, pos=graph_pos, edge_labels=edge_labels)

"""

# ============================
# MANUALLY CONSTRUCTING GRAPH
# ============================


def sp2_birth(N=15):
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(1, N) for j in range(1, N)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 2 * i + sp.absolute(5 * sp.randn())

    def q_death(i, j):
        return i + sp.absolute(5 * sp.randn())  # ** 2

    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i in range(1, N - 1) for j in range(1, N)]
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i in range(1, N) for j in range(1, N - 1)]

    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i in range(2, N) for j in range(1, N)]
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i in range(1, N) for j in range(2, N)]

    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]
    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)

    """
    # perturbing G edges with abs| N(0, [(Q_max - Q_min) / 10]^2) | noise
    # (and now we have a complete graph)
    Q = sp.asarray(nx.adjacency_matrix(G, weight='weight').todense())
    Q_per = Q + sp.absolute((sp.amax(Q) - sp.amin(Q)) / 10 * sp.randn(*Q.shape))
    sp.fill_diagonal(Q_per, 0)
    G = nx.DiGraph(Q_per)
    """

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


def sp2_react(N=15):
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(1, N) for j in range(1, N)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 2 * i + sp.absolute(0.5 * sp.randn())

    def q_death(i, j):
        return i + sp.absolute(0.5 * sp.randn())  # ** 2

    def q_react(i, j):
        # i + j = 2i
        return i * j + sp.absolute(0.5 * sp.randn())

    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i in range(1, N - 1) for j in range(1, N)]
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i in range(1, N) for j in range(1, N - 1)]

    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i in range(2, N) for j in range(1, N)]
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i in range(1, N) for j in range(2, N)]

    # i + j = 2i
    e_react = [((i, j), (i + 1, j - 1), {'weight': q_react(i, j)})
               for i in range(1, N - 1) for j in range(2, N)]

    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]
    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)
    G.add_edges_from(e_react)

    """
    # perturbing G edges with N(0, [(Q_max - Q_min) / 10]^2) noise
    # (and now we have a complete graph)
    Q = sp.asarray(nx.adjacency_matrix(
        G, weight='weight').todense())
    Q_per = Q + sp.absolute((sp.amax(Q) - sp.amin(Q)) / 10 * sp.randn(*Q.shape))
    sp.fill_diagonal(Q_per, 0)
    G = nx.DiGraph(Q_per)
    """

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


def sp2_2react(N=15):
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(1, N) for j in range(1, N)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 2 * i + sp.absolute(0.5 * sp.randn())

    def q_death(i, j):
        return i + sp.absolute(0.5 * sp.randn())  # ** 2

    def q_infect(i, j):
        # i + j = 2i
        return i * j + sp.absolute(0.5 * sp.randn())

    def q_annih(i, j):
        # i + j = 0
        return 0.5 * i * j + sp.absolute(0.5 * sp.randn())

    # i birth
    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i in range(1, N - 1) for j in range(1, N)]
    # j birth
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i in range(1, N) for j in range(1, N - 1)]

    # i death
    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i in range(2, N) for j in range(1, N)]
    # j death
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i in range(1, N) for j in range(2, N)]

    # i + j = 2i
    e_infect = [((i, j), (i + 1, j - 1), {'weight': q_infect(i, j)})
                for i in range(1, N - 1) for j in range(2, N)]

    # i + j = 0
    e_annih = [((i, j), (i - 1, j - 1), {'weight': q_annih(i, j)})
               for i in range(2, N) for j in range(2, N)]

    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]
    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)
    G.add_edges_from(e_infect)
    # G.add_edges_from(e_annih)

    """
    # perturbing G edges with N(0, [(Q_max - Q_min) / 10]^2) noise
    # (and now we have a complete graph)
    Q = sp.asarray(nx.adjacency_matrix(
        G, weight='weight').todense())
    Q_per = Q + sp.absolute((sp.amax(Q) - sp.amin(Q)) / 10 * sp.randn(*Q.shape))
    sp.fill_diagonal(Q_per, 0)
    G = nx.DiGraph(Q_per)
    """

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


def sp2_LV_perturbed(N=20):

    import Lotka_Volterra

    G, ode_grad, system_label = Lotka_Volterra.sp2_LV(N)

    # perturbing every edge with truncated Gaussian noise |eta|:
    # eta ~ N(0, n_sigma^2)
    n_sigma = 0.5
    for e in G.edges():
        G[e[0]][e[1]]['weight'] += sp.absolute(n_sigma * sp.randn())

    return G, None, system_label


def sp2_LV_reduced(N=20):
    import Lotka_Volterra

    G, ode_grad, system_label = Lotka_Volterra.sp2_LV(N)

    # randomly removing some edges
    p_rem = 0.02

    es = list(G.edges())
    ps = sp.rand(len(es))

    for e, p in zip(es, ps):
        if p < p_rem:
            G.remove_edge(*e)

    return G, None, system_label


def sp2_LV_perturbed_reduced(N=20):
    import Lotka_Volterra

    G, ode_grad, system_label = Lotka_Volterra.sp2_LV(N)

    # perturbing every edge with truncated Gaussian noise |eta|:
    # eta ~ N(0, n_sigma^2)
    n_sigma = 0.5
    for e in G.edges():
        G[e[0]][e[1]]['weight'] += sp.absolute(n_sigma * sp.randn())

    # randomly removing some edges
    p_rem = 0.02

    es = list(G.edges())
    ps = sp.rand(len(es))

    for e, p in zip(es, ps):
        if p < p_rem:
            G.remove_edge(*e)

    return G, None, system_label


if __name__ == '__main__':
    # G = sp2_birth(N=20)
    # G = sp2_react(N=20)
    # G = sp2_2react(N=15)
    G = sp2_birth(N=25)

    # Saving adjacency matrix of undirected pCTMC state graph
    G_undir = G.to_undirected()
    nodelist = G.nodes()

    A = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())
    A = sp.int_(A)
    sp.savetxt('sp2_2react.csv', A, fmt='%d', delimiter=',')

    Poynting, GPR, Poynting_pred = embed_ss.embed_graph(
        G, proj_dim=3, plot_edges=True)
    plt.title("State projection on continuous domain and imputed vector field.")
    plt.show()
