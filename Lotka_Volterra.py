import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt


def sp2_LV(N=100):
    system_label = 'Lotka-Volterra'
    G = nx.DiGraph()
    omega = N  # system size
    # nodes

    nodes = [(i, j) for i in range(2 * N + 1) for j in range(N + 1)]
    print(len(G.nodes()))

    # e_prey_birth (a -> 2a)
    # e_consume (b + a -> 2b)
    # e_pred_death (b -> 0)

    def q_prey_birth(i, j):
        return 1 / 2 * i

    def q_consume(i, j):
        return 1 / 10 * i * j / omega

    # def q_pred_birth(i, j):
    #     return 0.01 * i * j / N

    def q_pred_death(i, j):
        return 1 / 3 * j

    # prey birth
    e_prey_birth = [((i, j), (i + 1, j), {'weight': q_prey_birth(i, j)})
                    for i, j in nodes if (i + 1, j) in nodes]
    # consumption
    e_consumption = [((i, j), (i - 1, j + 1), {'weight': q_consume(i, j)})
                     for i, j in nodes if (i - 1, j + 1) in nodes]
    # predator birth
    # e_pred_birth = [((i, j), (i, j + 1), {'weight': q_consume(i, j)})
    #                 for i, j in nodes if (i, j + 1) in nodes]
    # predator death
    e_pred_death = [((i, j), (i, j - 1), {'weight': q_pred_death(i, j)})
                    for i, j in nodes if (i, j - 1) in nodes]

    G.add_edges_from(e_prey_birth)
    G.add_edges_from(e_consumption)
    # G.add_edges_from(e_pred_birth)
    G.add_edges_from(e_pred_death)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G, None, system_label


if __name__ == '__main__':
    G, system_label = sp2_LV(N=30)

    # Saving adjacency matrix of undirected pCTMC state graph
    G_undir = G.to_undirected()
    nodelist = G.nodes()

    A = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())
    A = sp.int_(A)
    sp.savetxt('sp2_LV.csv', A, fmt='%d', delimiter=',')

    Poynting, GPR, Poynting_pred, Diffusion_matrix_dict = embed_ss.embed_graph(
        G, proj_dim=2, plot=True, plot_edges=True)

    plt.title('State embedding in continuous domain and imputed vector field.'
              f'\n({system_label})')
    plt.show()
