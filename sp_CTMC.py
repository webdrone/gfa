import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt

"""
================
GRID AUTO-GRAPH
================
population = [N, N]
G = nx.grid_graph(dim=population)
G = nx.DiGraph(G)

print(nx.adjacency_matrix(G).getnnz())
print(nx.adjacency_matrix(G))

graph_pos = nx.spectral_layout(G)
edge_labels = {(k1, k2): v for k1, k2, v in G.edges(data='weight')}
nx.draw_networkx(G, pos=graph_pos, edge_labels=edge_labels)

"""

# ============================
# MANUALLY CONSTRUCTING GRAPH
# ============================


def sp2_birth(N=15):
    system_label = 'Birth-death'
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(0, N + 1) for j in range(0, N + 1)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 10 / N

    def q_death(i, j):
        return 0.5 * i  # ** 2

    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i, j in G.nodes() if (i + 1, j) in G.nodes()]
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i, j in G.nodes() if (i, j + 1) in G.nodes()]

    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i, j in G.nodes() if (i - 1, j) in G.nodes()]
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i, j in G.nodes() if (i, j - 1) in G.nodes()]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)

    def ode_grad(x, t0):
        def d_sp1(x):
            return q_birth(*x) - q_death(*x)

        def d_sp2(x):
            return q_birth(*(x[::-1])) - q_death(*(x[::-1]))

        rates = [d_sp1, d_sp2]

        g = sp.array([r(x) for r in rates])
        return g

    return G, ode_grad, system_label


def sp2_react(N=15):
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(1, N) for j in range(1, N)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 2 * i

    def q_death(i, j):
        return i  # ** 2

    def q_react(i, j):
        # i + j = 2i
        return i * j

    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i in range(1, N - 1) for j in range(1, N)]
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i in range(1, N) for j in range(1, N - 1)]

    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i in range(2, N) for j in range(1, N)]
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i in range(1, N) for j in range(2, N)]

    # i + j = 2i
    e_react = [((i, j), (i + 1, j - 1), {'weight': q_react(i, j)})
               for i in range(1, N - 1) for j in range(2, N)]

    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]
    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)
    G.add_edges_from(e_react)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


def sp2_2react(N=15):
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(1, N) for j in range(1, N)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 2 * i

    def q_death(i, j):
        return i  # ** 2

    def q_infect(i, j):
        # i + j = 2i
        return i * j

    def q_annih(i, j):
        # i + j = 0
        return 0.5 * i * j

    # i birth
    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i in range(1, N - 1) for j in range(1, N)]
    # j birth
    e_birth += [((i, j), (i, j + 1), {'weight': q_birth(j, i)})
                for i in range(1, N) for j in range(1, N - 1)]

    # i death
    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i in range(2, N) for j in range(1, N)]
    # j death
    e_death += [((i, j), (i, j - 1), {'weight': q_death(j, i)})
                for i in range(1, N) for j in range(2, N)]

    # i + j = 2i
    e_infect = [((i, j), (i + 1, j - 1), {'weight': q_infect(i, j)})
                for i in range(1, N - 1) for j in range(2, N)]

    # i + j = 0
    e_annih = [((i, j), (i - 1, j - 1), {'weight': q_annih(i, j)})
               for i in range(2, N) for j in range(2, N)]

    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]
    # e_birth += [((N, j), (N, j + 1), {'weight': q_birth}) for j in range(N - 1)]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)
    # G.add_edges_from(e_infect)
    # G.add_edges_from(e_annih)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)
    return G


def sp2_gene_switch(N=50):
    system_label = 'Gene switch'
    G = nx.DiGraph()

    # nodes
    G.add_nodes_from([(i, j) for i in range(0, N + 1) for j in (0, 1)])
    print(len(G.nodes()))

    # edges
    # q_birth = 2
    # q_death = 1
    def q_birth(i, j):
        return 0.1 * (10 * j + 1 * (1 - j))

    def q_death(i, j):
        return 0.05 * i

    def q_switch(i, j):
        return 0.005

    e_birth = [((i, j), (i + 1, j), {'weight': q_birth(i, j)})
               for i, j in G.nodes() if (i + 1, j) in G.nodes()]
    e_death = [((i, j), (i - 1, j), {'weight': q_death(i, j)})
               for i, j in G.nodes() if (i - 1, j) in G.nodes()]
    e_switch = [((i, j), (i, j + 1), {'weight': q_switch(i, j)})
                for i, j in G.nodes() if (i, j + 1) in G.nodes()]
    e_switch += [((i, j), (i, j - 1), {'weight': q_switch(i, j)})
                 for i, j in G.nodes() if (i, j - 1) in G.nodes()]

    G.add_edges_from(e_birth)
    G.add_edges_from(e_death)
    G.add_edges_from(e_switch)

    n_nodes = len(G.nodes())
    n_edges = len(G.edges())

    print("n_nodes:", n_nodes)
    print("edges:", n_edges)

    ode_grad = None

    return G, ode_grad, system_label


if __name__ == '__main__':
    # G = sp2_birth(N=20)
    # G = sp2_react(N=20)
    # G = sp2_2react(N=15)
    G = sp2_birth(N=25)

    # Saving adjacency matrix of undirected pCTMC state graph
    G_undir = G.to_undirected()
    nodelist = G.nodes()

    A = sp.asarray(nx.adjacency_matrix(
        G_undir, nodelist=nodelist, weight=None).todense())
    A = sp.int_(A)
    sp.savetxt('sp2_2react.csv', A, fmt='%d', delimiter=',')

    Poynting, GPR, Poynting_pred = embed_ss.embed_graph(
        G, proj_dim=3, plot_edges=True)
    plt.title("State projection on continuous domain and imputed vector field.")
    plt.show()
