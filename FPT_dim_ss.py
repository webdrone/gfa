import scipy as sp
from scipy import linalg as la
from scipy.integrate import odeint

import sdeint
from sdeint import itoint

import matplotlib as mpl
import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

import networkx as nx
# sklearn GPs
# '''
from sklearn.gaussian_process import GaussianProcessRegressor
import sklearn.gaussian_process.kernels as kernels


import embed_ss
import simulate_ss
import compare_ss


# Checking hitting time for CTMC trajectories
def fpt_ctmc(trajs, barrier):
    n_traj = len(trajs)
    times = sp.zeros(n_traj)
    for i, tr in enumerate(trajs):
        # if the trajectory doesn't reach within given time, mark with -1
        times[i] = -1
        for t, s in tr:
            if s in barrier:
                times[i] = t
                break
    return times


# Checking hitting time for fluid trajectory
def fpt_fluid(traj, barrier, eps=None, graph_pos=None):
    t_lin = traj[:, 0]
    traj_s = traj[:, 1:]
    for t, s in zip(t_lin, traj_s):
        if eps is not None:
            barrier_pos = sp.array([graph_pos[idx] for idx in barrier])
            diff = barrier_pos - s
            diff = la.norm(diff, axis=1)
            if sp.any(diff < eps):
                return t
        elif eps is None and graph_pos is not None:
            diff = graph_pos - s
            diff = la.norm(diff, axis=1)
            idx_min = sp.argmin(diff)
            if idx_min in barrier:
                return t
    else:
        return -1


# GP for v field
def gp_ode_func(x, t0, GPR):
    # GPflow GPR
    # v_pred, var = GPR.predict_y(sp.array([x]))
    x_pred = sp.atleast_2d(x)
    # x_pred = x_pred[:, :-1]

    # sklearn GPR
    v_pred = GPR.predict(x_pred)
    """
    # v_fpt_pred = GPR_fpt.predict(x_pred)
    # v_fpt_pred = sp.exp(v_fpt_pred - 5)
    # v_fpt_pred = (1 - x[:, -1]) * v_fpt_pred
    # v_pred = sp.column_stack((v_pred, v_fpt_pred))
    #"""
    sp.squeeze(v_pred)

    return v_pred


if __name__ == '__main__':
    # """
    # LOADING CTMC
    # G, Q, nodelist = compare_ss.load_PEPA()
    G, Q, nodelist = compare_ss.load_pCTMC()

    # Embedding in \mathbb{R}^{n_dim}
    n_dim = 2
    N = len(nodelist)
    # """

    print()
    print("Embedding graph and GP for gradient field...")

    # BARRIER SET
    # """
    # pCTMCs
    barrier_indices = [nodelist.index((i, j))
                       for i in range(15, 23) for j in range(15, 23)]
    # barrier_indices = sp.random.randint(0, high=N, size=N // 20)
    # PEPA
    # barrier_indices = sp.arange(3 * N // 4, N)

    # Lumping barrier states
    abs_node = nodelist[barrier_indices[0]]
    subset_idx = [i for i in range(N) if i not in barrier_indices]
    subset_nodes = [nodelist[i] for i in subset_idx]
    G_subset = nx.DiGraph(G.subgraph(subset_nodes))
    abs_edges = [(n1, abs_node,
                  G.out_degree(n1, weight='weight') - G_subset.out_degree(n1, weight='weight'))
                 for n1, n2 in G.edges(subset_nodes) if n2 not in subset_nodes]
    G_subset.add_weighted_edges_from(abs_edges)

    Q_subset = sp.asarray(nx.adjacency_matrix(
        G_subset, nodelist=subset_nodes + [abs_node]).todense())
    sp.fill_diagonal(Q_subset, -sp.sum(Q_subset -
                                       sp.diag(sp.diag(Q_subset)), axis=1))

    graph_pos, GPR, Poynting_vec_field, Diffusion_matrix_dict = embed_ss.embed_graph(
        G_subset, proj_dim=n_dim, nodelist=subset_nodes + [abs_node], plot=False)

    # print(graph_pos)
    # print(nodelist)
    # print()
    # print("Checking Q diagonals:", sp.sum(Q), sp.sum(Q_subset))
    # print(sp.amin(sp.diag(Q_subset - Q[sp.array(subset_idx), sp.array(subset_idx)])))

    # Constructing barrier drift training data
    """
    barrier_drift_field = sp.vstack([graph_pos[n] for n in subset_nodes])
    barrier_drift = sp.diag(Q_subset) - sp.diag(Q)[sp.array(subset_idx)]
    barrier_drift[barrier_drift <= 0] = 1e-5
    barrier_drift_field = sp.column_stack((barrier_drift_field, barrier_drift))

    print(sp.amin(barrier_drift_field[:, -1]),
          sp.amax(barrier_drift_field[:, -1]))

    # GPR_fpt
    kern_rbf = kernels.RBF(length_scale=1e-2 * sp.ones(n_dim))
    kern_ampl = kernels.ConstantKernel(constant_value=1)
    kern = kernels.Product(kern_ampl, kern_rbf)

    # 2. * RBF(length_scale=sp.array([1.] * proj_dim))
    # rbf_kern = None
    GPR_fpt = GaussianProcessRegressor(kernel=kern,
                                       alpha=1e-1,
                                       optimizer=None,
                                       normalize_y=False,
                                       # n_restarts_optimizer=2,
                                       )
    barrier_drift_log = sp.log(barrier_drift_field[:, -1]) + 5
    GPR_fpt.fit(barrier_drift_field[:, :-1], barrier_drift_log)
    #"""

    # DRAWING MULTIPLE TRAJECTORIES (DISCRETE SS)
    n_traj = 1 * 10 ** 2
    t_end = 1
    # sp.array([sp.arange(10)] * 100).flatten()
    # s_0 = N // 5  # - int(sp.sqrt(N))
    # s_0 = nodelist.index((12, 10))
    s_0 = sp.random.choice(sp.array(subset_idx))

    print()
    print("Simulating from Q, n_traj =", n_traj)

    trajs = simulate_ss.simulate_Q(Q, s_0=s_0, time_end=t_end, n_traj=n_traj)

    # PROBABILITY DISTRIBUTION OVER STATES IN TIME (DISCRETE SS)
    ticks = 100
    t_lin = sp.linspace(0, t_end, ticks)

    """
    p_ss, trajs = simulate_ss.probability_ss(trajs, N=N, t_lin=t_lin)
    print("Probability over SS sum, should be 1")
    print(sp.amin(sp.sum(p_ss, axis=1)), sp.amax(sp.sum(p_ss, axis=1)))

    # PROJECTING DISCRETE P_ss TO CONTINUOUS DOMAIN
    p_R_ss = sp.zeros((ticks, n_dim))
    p_R_ss = p_ss @ sp.array([graph_pos[i] for i in nodelist])
    # """

    """
    # PROJECTING DISCRETE TRAJS TO CONTINUOUS DOMAIN
    trajs_ctmc = [sp.zeros((ticks, n_dim))] * n_traj
    for i in range(n_traj):
        trajs_ctmc[i] = sp.array(
            [graph_pos[nodelist[int(s)]] for s in trajs[i]])
    # """

    # FLUID INTEGRATION PATHS
    traj_fluid = sp.zeros((ticks, n_dim))
    trajs_sde = []
    if (not hasattr(s_0, '__iter__')):
        s_0 = sp.array([s_0])
    for s in s_0:
        s_pos = graph_pos[nodelist[s]]
        # print(nodelist[s], gp_ode_func(s_pos),
        #       sp.sum([sp.array(graph_pos[n2] - graph_pos[n1])
        #               * G[n1][n2]['weight'] for n1, n2 in nx.edges(G, nodelist[s])], axis=0))
        # print(s_pos)

        # SCIPY ODE INTEGRATION for Fluid SS
        print()
        print("fluid integration...")

        traj_fluid += odeint(compare_ss.gp_ode_func, s_pos, t_lin, args=(GPR,))

        # EULER INTEGRATION for Fluid SS
        # traj_fluid += compare_ss.eulerint(gp_ode_func, s_pos, t_lin)

        # SDEINT Itô SDE integration
        # trajs_sde += [itoint(gp_ode_func, sigma, s_pos, t_lin)
        #               for _ in range(5)]
    traj_fluid /= len(s_0)
    traj_fluid = sp.column_stack((t_lin, traj_fluid))

    # Embedded barrier points
    """
    barrier_pos = [graph_pos[nodelist[idx]] for idx in barrier_indices]
    graph_pos_arr = sp.array([graph_pos[i] for i in nodelist])

    fpt_fluid_res = fpt_fluid(
        traj=traj_fluid, barrier=barrier_indices, graph_pos=graph_pos_arr)
    # """

    fpt_ctmc_res = fpt_ctmc(trajs=trajs, barrier=barrier_indices)

    # PLOTTING
    fig, ax = plt.subplots()

    # Plot cumulative histogram
    n_bins = 1000
    n, bins, _ = ax.hist(fpt_ctmc_res, n_bins, density=True,
                         histtype='step', cumulative=True,
                         label='Empirical FPT', color='b')

    """
    # Vertical line showing the FPT of fluid trajectory
    n, bins, _ = ax.hist(fpt_fluid_res, bins, density=True,
                         histtype='step', cumulative=True,
                         label='Fluid FPT', color='r')
    # """

    # FPT drift trajectory
    ax.plot(traj_fluid[:, 0], traj_fluid[:, 1], label='FPT drift', color='r')

    # ax.axvline(x=fpt_fluid_res, c='r', label='Fluid FPT')
    ax.legend()
    ax.set_xlabel('time (s)')
    ax.set_ylabel('CDF')

    # Plotting embedding
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # edges
    edges = G_subset.edges()
    for ed in edges:
        ax.plot(*zip(graph_pos[ed[0]][:], graph_pos[ed[1]][:]),
                color='grey', linewidth=0.5)
    # barrier nodes
    # nodes
    graph_pos_arr = sp.array([graph_pos[n] for n in subset_nodes])
    ax.scatter(graph_pos_arr[:, 0], graph_pos_arr[:, 1],
               s=2 * sp.array([G_subset[n][abs_node]['weight']
                               if G_subset[n].get(abs_node) is not None
                               else 0
                               for n in subset_nodes]),
               marker='o',
               c='r')

    # fluid trajectory
    import matplotlib.cm as cm
    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    fluid_c = cm.ScalarMappable(norm=norm, cmap=cm.get_cmap(name='gist_heat'))
    for t in range(len(traj_fluid)):
        label = None
        if t == 0:
            label = 'Fluid trajectory'
        ax.plot(traj_fluid[t:t + 2, 1], traj_fluid[t: t + 2, 2],
                label=label,
                color=fluid_c.to_rgba(traj_fluid[t, 1]))
    # fig.colorbar(ax)

    ax.legend()
    plt.show()
