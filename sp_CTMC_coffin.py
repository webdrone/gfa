import scipy as sp
import embed_ss
import networkx as nx
import matplotlib.pyplot as plt

"""
================
GRID AUTO-GRAPH
================
population = [N, N]
G = nx.grid_graph(dim=population)
G = nx.DiGraph(G)

print(nx.adjacency_matrix(G).getnnz())
print(nx.adjacency_matrix(G))

graph_pos = nx.spectral_layout(G)
edge_labels = {(k1, k2): v for k1, k2, v in G.edges(data='weight')}
nx.draw_networkx(G, pos=graph_pos, edge_labels=edge_labels)

"""


def build_subset(G, root=1, radius=1):
    G_undir = G.to_undirected()
    paths = nx.single_source_shortest_path(G_undir, root, cutoff=radius)
    subset_nodes = paths.keys()
    G_subset = nx.DiGraph(G.subgraph(subset_nodes))
    return G_subset


def build_coffin(G, root=1, radius=1):
    G_undir = G.to_undirected()
    paths = nx.single_source_shortest_path(G_undir, root, cutoff=radius)
    subset_nodes = paths.keys()
    G_coffin = nx.DiGraph(G.subgraph(subset_nodes))

    # """
    for e in G.edges(data=True):
        if e[0] in subset_nodes and e[1] not in subset_nodes:
            G_coffin.add_edge(e[0], (e[0], 0), weight=0)
            G_coffin.add_edge((e[0], 0), e[0], weight=0)
        if e[1] in subset_nodes and e[0] not in subset_nodes:
            G_coffin.add_edge(e[1], (e[1], 0), weight=0)
            G_coffin.add_edge((e[1], 0), e[1], weight=0)
    for e in G.edges(data=True):
        if e[0] in subset_nodes and e[1] not in subset_nodes:
            G_coffin[e[0]][(e[0], 0)]['weight'] += G[e[0]][e[1]]['weight']
        if e[0] not in subset_nodes and e[1] in subset_nodes:
            G_coffin[(e[1], 0)][e[1]]['weight'] += G[e[0]][e[1]]['weight']
    # """
    return G_coffin



def build_subset_sp2(G, root=(1, 1), radius=1):
    G_undir = G.to_undirected()
    paths = nx.single_source_shortest_path(G_undir, root, cutoff=radius)
    subset_nodes = paths.keys()
    G_subset = nx.DiGraph(G.subgraph(subset_nodes))
    return G_subset


def build_coffin_sp2(G, root=(1, 1), radius=1):
    G_undir = G.to_undirected()
    paths = nx.single_source_shortest_path(G_undir, root, cutoff=radius)
    subset_nodes = paths.keys()
    G_coffin = nx.DiGraph(G.subgraph(subset_nodes))

    # """
    for e in G.edges(data=True):
        if e[0] in subset_nodes and e[1] not in subset_nodes:
            G_coffin.add_edge(e[0], (e[0], 0), weight=0)
            G_coffin.add_edge((e[0], 0), e[0], weight=0)
        if e[1] in subset_nodes and e[0] not in subset_nodes:
            G_coffin.add_edge(e[1], (e[1], 0), weight=0)
            G_coffin.add_edge((e[1], 0), e[1], weight=0)
    for e in G.edges(data=True):
        if e[0] in subset_nodes and e[1] not in subset_nodes:
            G_coffin[e[0]][(e[0], 0)]['weight'] += G[e[0]][e[1]]['weight']
        if e[0] not in subset_nodes and e[1] in subset_nodes:
            G_coffin[(e[1], 0)][e[1]]['weight'] += G[e[0]][e[1]]['weight']
    # """
    return G_coffin
